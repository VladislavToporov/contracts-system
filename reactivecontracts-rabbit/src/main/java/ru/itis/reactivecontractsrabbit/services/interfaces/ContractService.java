package ru.itis.reactivecontractsrabbit.services.interfaces;

import ru.itis.reactivecontractsrabbit.dto.ContractDTO;


public interface ContractService {

    void sendContract(ContractDTO contractDTO);

}
