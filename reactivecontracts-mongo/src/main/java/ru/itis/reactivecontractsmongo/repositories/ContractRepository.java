package ru.itis.reactivecontractsmongo.repositories;

import ru.itis.reactivecontractsmongo.models.Contract;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;


public interface ContractRepository extends ReactiveMongoRepository<Contract, String> {

    Mono<Contract> findByTitle(String title);

}
