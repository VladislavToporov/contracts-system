package ru.itis.reactivecontractsmongo.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@ComponentScan("ru.itis.reactivecontractsmongo")
@EnableReactiveMongoRepositories(basePackages = {"ru.itis.reactivecontractsmongo.repositories"})
@EntityScan(basePackages = {"ru.itis.reactivecontractsmongo.models"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
