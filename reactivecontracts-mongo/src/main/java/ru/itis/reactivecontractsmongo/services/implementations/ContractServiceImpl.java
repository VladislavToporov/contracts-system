package ru.itis.reactivecontractsmongo.services.implementations;

import ru.itis.reactivecontractsmongo.models.Contract;
import ru.itis.reactivecontractsmongo.dto.ContractDTO;
import ru.itis.reactivecontractsmongo.dto.ContractRedisDTO;
import ru.itis.reactivecontractsmongo.repositories.ContractRepository;
import ru.itis.reactivecontractsmongo.services.interfaces.ContractService;
import ru.itis.reactivecontractsmongo.utils.EntityMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ContractServiceImpl implements ContractService {

    private final ContractRepository contractRepository;
    private final WebClient webClient;

    @Autowired
    public ContractServiceImpl(ContractRepository contractRepository, WebClient webClient) {
        this.contractRepository = contractRepository;
        this.webClient = webClient;
    }

    @Override
    public Mono<ContractDTO> getContractByTitle(String title) {

        Mono<Contract> contract = this.contractRepository.findByTitle(title);

        return contract.map(EntityMapper::mapContract);
    }

    @Override
    public Mono<ContractDTO> saveContract(ContractDTO contractDTO) {

        Contract contract = Contract.builder()
                                    .title(contractDTO.getTitle())
                                    .priceOfContract(contractDTO.getPriceOfContract())
                                    .contractDuration(contractDTO.getContractDuration())
                                    .activeContractStatus(contractDTO.getActiveContractStatus())
                                    .build();

        ContractRedisDTO contractRedisDTO = new ContractRedisDTO();

        Mono<Contract> savedContract = this.contractRepository.save(contract);
        Mono<ContractDTO> resultDTO = savedContract.map(EntityMapper::mapContract);

        savedContract.map(contract1 -> {
            contractRedisDTO.setId(contract1.getId());
            contractRedisDTO.setContractDuration(contract1.getContractDuration());
            return Mono.just(contractRedisDTO);
        }).subscribe();

        this.webClient.post()
                      .uri("/save")
                      .body(BodyInserters.fromObject(contractRedisDTO))
                      .exchange()
                      .subscribe();

        return resultDTO;
    }

    @Override
    public Flux<ContractDTO> getAllContracts() {

        Flux<Contract> contracts = this.contractRepository.findAll();

        return contracts.map(EntityMapper::mapContract);
    }
}
