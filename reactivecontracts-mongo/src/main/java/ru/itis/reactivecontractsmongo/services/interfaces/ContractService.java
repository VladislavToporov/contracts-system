package ru.itis.reactivecontractsmongo.services.interfaces;

import ru.itis.reactivecontractsmongo.dto.ContractDTO;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ContractService {

    Mono<ContractDTO> getContractByTitle(String title);

    Mono<ContractDTO> saveContract(ContractDTO contractDTO);

    Flux<ContractDTO> getAllContracts();

}
