package ru.itis.reactivecontractsmongo.utils;

import ru.itis.reactivecontractsmongo.models.Contract;
import ru.itis.reactivecontractsmongo.dto.ContractDTO;

public class EntityMapper {

    public static ContractDTO mapContract(Contract contract){

        return ContractDTO.builder()
                          .title(contract.getTitle())
                          .priceOfContract(contract.getPriceOfContract())
                          .contractDuration(contract.getContractDuration())
                          .activeContractStatus(contract.getActiveContractStatus())
                          .build();

    }

}
