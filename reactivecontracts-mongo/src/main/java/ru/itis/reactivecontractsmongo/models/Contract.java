package ru.itis.reactivecontractsmongo.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.ToString;

import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
@Document(collection = "contracts")
@TypeAlias("contract")
public class Contract {

    @Id
    private String id;

    @Field("title")
    private String title;

    @Field("price_of_contract")
    private Long priceOfContract;

    @Field("contract_duration")
    private Long contractDuration;

    @Field("active_contract")
    private Boolean activeContractStatus;

}
