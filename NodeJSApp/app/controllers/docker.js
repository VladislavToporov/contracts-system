const os = require('os-utils');
const axios = require('axios');
const http = require('http');

exports.indexPage = (req, resp) => {

    const freeMem = os.freemem();
    const totalMem = os.totalmem();


    const projectInfo = "http://localhost:5000/api/v1/projects";
    const projectDetails = projectInfo + "/contract-launch";
	console.log("1");


  
    var containersList = [
{"name":"contractlaunch_reactivecontracts-mongo_1", "ports":27017},
{"name":"contractlaunch_reactivecontracts-redis_1", "ports":6379},
{"name":"contractlaunch_reactivecontracts-rabbit_1", "ports": 15671},
{"name":"contractlaunch_environment-mongo_1", "ports":80881 },
{"name":"contractlaunch_environment-redis_1", "ports":8082 },
{"name":"contractlaunch_environment-rabbit_1", "ports":8083},

];
  var activeProjectsCount = containersList.length;


 http
    .get(projectInfo, res => {
        console.log("1.5");
             activeProjectsCount = 4; //res.data.active.length;
	console.log(activeProjectsCount);
    })
    .on('error', err => {
         console.error(err);
    });

	console.log("2");


 http
    .get(projectDetails, res => {
       //containersList = res.data;
		console.log(res.data);
    })
    .on('error', err => {
         console.error(err);
    });

	console.log("2");
   



    resp.render('index_page', {
        'title': 'Управление контейнерами',
        'free_mem': freeMem,
        'total_mem': totalMem,
        'active_count': activeProjectsCount,
        'containers': containersList
    })

};
