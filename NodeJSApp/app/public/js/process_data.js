$(document).ready(function(){
	   	var dict;
		var rabbit_counter = 1; 
		function inc() {
			rabbit_counter++;
		}
            	var ajax_url =  "http://localhost:5000/api/v1/projects";
                $.ajax({
                    type: "GET",
                    contentType: "application/json",
                    url: ajax_url,
                    crossDomain: true,
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Methods': '*'
                    },
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        console.log(data);
	
			$("#num").html("Запущено " + (data.active.length - 1) + " контейнеров");

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });

		
		var ajax_url =  "http://localhost:5000/api/v1/projects/contract-launch";
                $.ajax({
                    type: "GET",
                    contentType: "application/json",
                    url: ajax_url,
                    crossDomain: true,
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type',
                        'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS'
                    },
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        console.log(data);
			$("#table").html("");
			for (var i = 0; i < data.containers.length; i++) {
				if(data.containers[i].is_running === true) {
				//dict[data.containers[i].name] = data.containers[i]['labels']['com.docker.compose.container-number'];
				$("#table").append("<tr><td><input type='checkbox' name='record'/></td><td>"+ data.containers[i].name_without_project + "</td><td>"+ "ports: " + Object.keys(data.containers[i].ports).toString() + "; " + "hash: " + data.containers[i].labels['com.docker.compose.config-hash'] + "</td></tr>");
}
console.log(dict);
			}

                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });
         

    $(".add-row").click(function(){
        console.log("i am here");
	inc();
	console.log(rabbit_counter);
        var requestURL = "http://localhost:5000/api/v1/services";
	var obj = {
           service: "reactivecontracts-rabbit",
           project: "contract-launch",
           num: $(".number").val()
        };
	console.log(obj);

                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    url: requestURL,
 		    crossDomain: true,
                    headers: {
                       'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type',
                    'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS'
			},
                    data: JSON.stringify(obj),
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        console.log(data);
			//$("#table").append("<tr><td><input type='checkbox' name='record'/></td><td>"+ data.containers[i].name.split('-')[1] + "</td><td>"+ "ports: " + Object.keys(data.containers[i].ports).toString() + "; " + "hash: " + data.containers[i].labels['com.docker.compose.config-hash'] + "</td></tr>");
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });

        window.location.replace("http://localhost:8080/index");
    });


    $(".delete-row").click(function(){
	rabbit_counter--;
	console.log(rabbit_counter);
        var requestURL = "http://localhost:5000/api/v1/services";
	var obj = {
                    service: "reactivecontracts-rabbit",
                    project: "contract-launch",
                    num: $(".number").val()
                };
        
		
                $.ajax({
                    type: "PUT",
                    contentType: "application/json",
                    url: requestURL,
 		    crossDomain: true,
                    headers: {
                       'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type',
                    'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS'
                    },
                    data: JSON.stringify(obj),
                    dataType: 'json',
                    cache: false,
                    timeout: 600000,
                    success: function (data) {
                        console.log(data);
                    },
                    error: function (e) {
                        console.log("ERROR : ", e);
                    }
                });

                window.location.replace("http://localhost:8080/index");
      
    });
});
