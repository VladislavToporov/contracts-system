package ru.itis.reactivecontractsredis.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.itis.reactivecontractsredis.models.Contract;

import ru.itis.reactivecontractsredis.messaging.subscriber.ContractInfoSubscriberImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    private final ObjectMapper objectMapper;
    private final ReactiveRedisConnectionFactory factory;

    @Autowired
    public RedisConfig(ObjectMapper objectMapper, ReactiveRedisConnectionFactory factory) {
        this.objectMapper = objectMapper;
        this.factory = factory;
    }

    @Bean
    public ReactiveRedisOperations<String, Contract> redisOperations(ReactiveRedisConnectionFactory factory){

        Jackson2JsonRedisSerializer<Contract> serializer = new Jackson2JsonRedisSerializer<>(Contract.class);

        RedisSerializationContext.RedisSerializationContextBuilder<String, Contract> builder =
                                RedisSerializationContext.newSerializationContext(new StringRedisSerializer());

        RedisSerializationContext<String, Contract> context = builder.value(serializer).build();

        return new ReactiveRedisTemplate<>(factory, context);

    }

    @Bean
    public ChannelTopic channelTopic(){
        return new ChannelTopic("contract-messaging");
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter(){
        return new MessageListenerAdapter(new ContractInfoSubscriberImpl(redisOperations(this.factory), this.objectMapper));
    }

}
