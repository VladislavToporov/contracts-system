package ru.itis.reactivecontractsredis.messaging.publisher;

import ru.itis.reactivecontractsredis.models.Contract;


public interface ContractInfoPublisher {

    void publish();
    void setContract(Contract contract);

}
