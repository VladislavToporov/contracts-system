package ru.itis.reactivecontractsredis.messaging.publisher;

import ru.itis.reactivecontractsredis.models.Contract;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

@Component
public class ContractInfoPublisherImpl implements ContractInfoPublisher{

    private Contract contract;

    private final ReactiveRedisOperations<String, Contract> contractOperations;
    private final ChannelTopic channelTopic;

    @Autowired
    public ContractInfoPublisherImpl(ReactiveRedisOperations<String, Contract> contractOperations, ChannelTopic channelTopic) {
        this.contractOperations = contractOperations;
        this.channelTopic = channelTopic;
    }

    @Override
    public void publish() {
        this.contractOperations.convertAndSend(this.channelTopic.getTopic(), this.contract)
                               .subscribe();
    }

    @Override
    public void setContract(Contract contract) {
        this.contract = contract;
    }
}
