package ru.itis.reactivecontractsredis.services.implementations;

import ru.itis.reactivecontractsredis.models.Contract;
import ru.itis.reactivecontractsredis.messaging.publisher.ContractInfoPublisher;
import ru.itis.reactivecontractsredis.services.interfaces.ContractService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ContractServiceImpl implements ContractService {

    private final ReactiveRedisOperations<String, Contract> contractOperations;
    private final ContractInfoPublisher contractInfoPublisher;

    @Autowired
    public ContractServiceImpl(ReactiveRedisOperations<String, Contract> contractOperations,
                               ContractInfoPublisher contractInfoPublisher) {
        this.contractOperations = contractOperations;
        this.contractInfoPublisher = contractInfoPublisher;
    }

    @Override
    public Flux<Contract> getAllContracts() {
        return this.contractOperations.keys("*")
                                      .flatMap(this.contractOperations.opsForValue()::get);
    }

    @Override
    public Mono<Contract> getContractByID(String id) {
        return this.contractOperations.opsForValue().get(id);
    }

    @Override
    public Mono<String> sendToRedis(Contract contract) {

        this.contractInfoPublisher.setContract(contract);
        this.contractInfoPublisher.publish();

        return Mono.just("{\"message\": \"Контракт отправлен в Redis\"}");

    }
}
