package ru.itis.reactivecontractsredis.services.interfaces;

import ru.itis.reactivecontractsredis.models.Contract;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ContractService {

    Flux<Contract> getAllContracts();
    Mono<Contract> getContractByID(String id);
    Mono<String> sendToRedis(Contract contract);

}
